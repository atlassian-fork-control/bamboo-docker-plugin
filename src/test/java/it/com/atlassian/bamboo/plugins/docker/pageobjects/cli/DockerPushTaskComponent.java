package it.com.atlassian.bamboo.plugins.docker.pageobjects.cli;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.AdvancedOptionsElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Map;

import static com.atlassian.bamboo.pageobjects.utils.PageElementFunctions.binder;

public class DockerPushTaskComponent implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerPushTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Docker";

    public static final String DOCKER_COMMAND_OPTION_PUSH = "push";

    public static final String DOCKER_COMMAND_OPTION = "commandOption";
    public static final String REPOSITORY = "pushRepository";
    public static final String REGISTRY_OPTION = "registryOption";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String CREDENTIALS_SOURCE = "pushCredentialsSource";
    public static final String SHARED_CREDENTIALS = "pushSharedCredentialsId";

    public static final String CFG_WORKING_SUB_DIRECTORY = "workingSubDirectory";
    public static final String CFG_ENVIRONMENT_VARIABLES = "environmentVariables";

    // ------------------------------------------------------------------------------------------------- Type Properties
    @ElementBy(name = DOCKER_COMMAND_OPTION)
    private SelectElement dockerCommandField;

    @ElementBy(name = REPOSITORY)
    private TextElement repositoryField;

    @ElementBy(id = "pushCredentialsSourceSHARED_CREDENTIALS")
    private PageElement credentialsSourceIsSharedCredentials;

    @ElementBy(name = USERNAME)
    private TextElement usernameField;

    @ElementBy(name = PASSWORD)
    private TextElement passwordField;

    @ElementBy(name = SHARED_CREDENTIALS)
    private SelectElement sharedCredentials;

    @ElementBy(name = EMAIL)
    private TextElement emailField;

    @ElementBy(name = CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @ElementBy(id = "advancedOptionsSection")
    private PageElement advancedOptions;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        getAdvancedOptionsElement().expand();

        dockerCommandField.select(Options.value(DOCKER_COMMAND_OPTION_PUSH));

        if (config.containsKey(REGISTRY_OPTION))
        {
            for (PageElement radio : elementFinder.findAll(By.name(REGISTRY_OPTION)))
            {
                if (config.get(REGISTRY_OPTION).equals(radio.getValue()))
                {
                    radio.click();
                    break;
                }
            }
        }
        if (config.containsKey(REPOSITORY))
        {
            repositoryField.setText(config.get(REPOSITORY));
        }
        if (config.containsKey(CREDENTIALS_SOURCE))
        {
            if ("SHARED_CREDENTIALS".equals(config.get(CREDENTIALS_SOURCE)))
            {
                credentialsSourceIsSharedCredentials.click();
            }
        }
        if (config.containsKey(SHARED_CREDENTIALS))
        {
            sharedCredentials.select(Options.value(config.get(SHARED_CREDENTIALS)));
        }
        if (config.containsKey(USERNAME))
        {
            usernameField.setText(config.get(USERNAME));
        }
        if (config.containsKey(PASSWORD))
        {
            passwordField.setText(config.get(PASSWORD));
        }
        if (config.containsKey(EMAIL))
        {
            emailField.setText(config.get(EMAIL));
        }
        if (config.containsKey(CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(CFG_ENVIRONMENT_VARIABLES));
        }
    }

    private AdvancedOptionsElement getAdvancedOptionsElement()
    {
        return binder(pageBinder, AdvancedOptionsElement.class).apply(advancedOptions);
    }
}