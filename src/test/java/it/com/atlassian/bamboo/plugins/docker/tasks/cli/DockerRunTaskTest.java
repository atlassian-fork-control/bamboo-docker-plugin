package it.com.atlassian.bamboo.plugins.docker.tasks.cli;

import com.atlassian.bamboo.matcher.IterablesTimesMatcher;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ArtifactConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.result.JobResultViewLogs;
import com.atlassian.bamboo.pageobjects.pages.tasks.ScriptTaskComponent;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.TestJobDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.cli.DockerRunTaskComponent;
import it.com.atlassian.bamboo.plugins.docker.tasks.AbstractDockerTaskTest;
import junitparams.JUnitParamsRunner;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Map;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class DockerRunTaskTest extends AbstractDockerTaskTest
{
    private static final String MINIMAL_IMAGE = "alpine";
    
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void testTask() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Docker task - the copy command proves the task working directory was mounted as a volume in the container
        final Map<String, String> dockerRunTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE)
                .put(DockerRunTaskComponent.WORK_DIR, "/taskTargetDir")
                .put(DockerRunTaskComponent.HOST_DIRECTORY_PREFIX + 0, "${bamboo.working.directory}/src")
                .put(DockerRunTaskComponent.CONTAINER_DATA_VOLUME_PREFIX + 0, "/taskSrcDir")
                .put(DockerRunTaskComponent.HOST_DIRECTORY_PREFIX + 1, "${bamboo.working.directory}/target")
                .put(DockerRunTaskComponent.CONTAINER_DATA_VOLUME_PREFIX + 1, "/taskTargetDir")
                .put(DockerRunTaskComponent.COMMAND, "cp /taskSrcDir/style.less style.css")
                .build();
        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run test task", dockerRunTaskConfig);

        // add artifact definition
        final ArtifactConfigurationPage artifactConfigurationPage = product.visit(ArtifactConfigurationPage.class, defaultJob.getKey());
        artifactConfigurationPage.createArtifactDefinition("Style Sheets", "target", "*.css");

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());
    }

    @Test
    public void testTaskExposedPortsAdditionalArgs() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Docker task - expose port 5000 and use -t additional arg to keep container running
        final Map<String, String> dockerRunTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE)
                .put(DockerRunTaskComponent.DETACH, "true")
                .put(DockerRunTaskComponent.NAME, "portargtest")
                .put(DockerRunTaskComponent.CONTAINER_PORT_PREFIX + 0, "5000")
                .put(DockerRunTaskComponent.HOST_PORT_PREFIX + 0, "5000")
                .put(DockerRunTaskComponent.ADDITIONAL_ARGS, "-t")
                .build();
        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run test task", dockerRunTaskConfig);

        // script task verifies container is still running and port 5000 has been exposed
        final Map<String, String> scriptTaskConfig = ImmutableMap.of(ScriptTaskComponent.CONFIG_BODY, "nc 127.0.0.1 5000 </dev/null");
        taskConfigurationPage.addNewTask(ScriptTaskComponent.getName(), ScriptTaskComponent.class, "Script task verifies open port", scriptTaskConfig);

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());
    }

    @Test
    public void testLinkToDetachedContainer() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Docker run task - start detached container
        final Map<String, String> detachTaskConfig = ImmutableMap.of(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE,
                                                                     DockerRunTaskComponent.DETACH, "true",
                                                                     DockerRunTaskComponent.NAME, "abc",
                                                                     DockerRunTaskComponent.COMMAND, "sleep 60");
        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run detach task", detachTaskConfig);

        // add Docker run task - link to detached container and output environment variables
        final Map<String, String> linkTaskConfig = ImmutableMap.of(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE,
                                                                   DockerRunTaskComponent.LINK, "true",
                                                                   DockerRunTaskComponent.COMMAND, "env");
        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run link task", linkTaskConfig);

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());

        // Verify the link was successful - env output should contain the ABC_NAME variable
        final JobResultViewLogs jobResultViewLogs = product.visit(JobResultViewLogs.class, plan.getDefaultJob(), 1);
        assertThat(jobResultViewLogs.getLog(), IterablesTimesMatcher.exactlyOnce(JobResultViewLogs.messageStartsWith("ABC_NAME", "build")));
    }

    @Test
    public void failBuildWhenServiceFailsToStart() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Docker task that will fail to start properly - Bamboo won't be able to connect to detached container
        final Map<String, String> dockerRunTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE)
                .put(DockerRunTaskComponent.DETACH, "true")
                .put(DockerRunTaskComponent.NAME, "failingTest")
                .put(DockerRunTaskComponent.CONTAINER_PORT_PREFIX + 0, "1234")
                .put(DockerRunTaskComponent.HOST_PORT_PREFIX + 0, "1234")
                .put(DockerRunTaskComponent.SERVICE_WAIT, "true")
                .put(DockerRunTaskComponent.SERVICE_TIMEOUT, "15")  //15 sec timeout
                .build();
        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run test task", dockerRunTaskConfig);

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitCompletion(plan.getKey());
        assertThat(backdoor.plans().getBuildResult(plan.getKey(), 1).isSuccessful(), is(false));
    }

    @Test
    public void runInexistentTagWillNotLockContainerName() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Docker task -
        final Map<String, String> dockerRunTaskConfig = ImmutableMap.of(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE + ":non-existent-tag",
                                                                        DockerRunTaskComponent.NAME, "my-container-name",
                                                                        DockerRunTaskComponent.DETACH, "true");
        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run test task", dockerRunTaskConfig);

        product.gotoHomePage();
        backdoor.plans().triggerBuild(plan.getKey());

        final Map<String, String> dockerRunTaskConfigNew = ImmutableMap.of(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE + ":latest",
                                                                           DockerRunTaskComponent.COMMAND, "tail -f /dev/null",
                                                                           DockerRunTaskComponent.NAME, "my-container-name",
                                                                           DockerRunTaskComponent.DETACH, "true");
        taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);
        taskConfigurationPage.editTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run test task", dockerRunTaskConfigNew);

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());
    }

    @Test
    public void testExpandingEnvironmentalVariables() throws Exception
    {
        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);


        final Map<String, String> dockerRunTaskConfig = ImmutableMap.<String, String>builder()
                .put(DockerRunTaskComponent.NAME, "Environmental variables")
                .put(DockerRunTaskComponent.IMAGE, MINIMAL_IMAGE)
                .put(DockerRunTaskComponent.DETACH, "false")
                .put(DockerRunTaskComponent.ENV_VARS,
                        "USER SOME_VAR=some_val bamboo_not_existing_environment_variable bamboo_build_working_directory")
                .put(DockerRunTaskComponent.COMMAND, "/bin/sh -c \"env|sort\"")
                .build();

        taskConfigurationPage.addNewTask(DockerRunTaskComponent.TASK_NAME, DockerRunTaskComponent.class, "Docker run detach task", dockerRunTaskConfig);

        product.gotoHomePage();
        backdoor.plans().triggerBuildAndAwaitSuccess(plan.getKey());

        final JobResultViewLogs jobResultViewLogs = product.visit(JobResultViewLogs.class, plan.getDefaultJob(), 1);
        //if environmental variables are present in log, there were passed properly and exist in shell in which docker is run
        assertThat(jobResultViewLogs.getLog(), hasItem(
                JobResultViewLogs.messageStartsWith("USER=", "build")));
        assertThat(jobResultViewLogs.getLog(), hasItem(
                JobResultViewLogs.messageStartsWith("bamboo_build_working_directory=", "build")));
        assertThat(jobResultViewLogs.getLog(), hasItem(JobResultViewLogs.buildMessageIs("SOME_VAR=some_val")));
        assertThat(jobResultViewLogs.getLog(), IterablesTimesMatcher.none(
                JobResultViewLogs.messageStartsWith("bamboo_not_existing_environment_variable", "build")));
    }

}