package it.com.atlassian.bamboo.plugins.docker.pageobjects.cli;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import it.com.atlassian.bamboo.plugins.docker.pageobjects.AdvancedOptionsElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.Map;

import static com.atlassian.bamboo.pageobjects.utils.PageElementFunctions.binder;

public class DockerPullTaskComponent implements TaskComponent
{
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerPullTaskComponent.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String TASK_NAME = "Docker";

    public static final String DOCKER_COMMAND_OPTION_PULL = "pull";

    public static final String DOCKER_COMMAND_OPTION = "commandOption";
    public static final String REPOSITORY = "pullRepository";
    public static final String REGISTRY_OPTION = "pullRegistryOption";
    public static final String USERNAME = "pullUsername";
    public static final String PASSWORD = "pullPassword";
    public static final String EMAIL = "pullEmail";
    public static final String CREDENTIALS_SOURCE = "pullCredentialsSource";
    public static final String SHARED_CREDENTIALS = "pullSharedCredentialsId";

    public static final String CFG_WORKING_SUB_DIRECTORY = "workingSubDirectory";
    public static final String CFG_ENVIRONMENT_VARIABLES = "environmentVariables";

    @ElementBy(name = DOCKER_COMMAND_OPTION)
    private SelectElement dockerCommandField;

    @ElementBy(name = REPOSITORY)
    private TextElement repositoryField;

    @ElementBy(id = "pullCredentialsSourceSHARED_CREDENTIALS")
    private PageElement credentialsSourceIsSharedCredentials;

    @ElementBy(name = USERNAME)
    private TextElement usernameField;

    @ElementBy(name = PASSWORD)
    private TextElement passwordField;

    @ElementBy(name = SHARED_CREDENTIALS)
    private SelectElement sharedCredentials;

    @ElementBy(name = EMAIL)
    private TextElement emailField;

    @ElementBy(name = CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @ElementBy(id = "advancedOptionsSection")
    private PageElement advancedOptions;

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected PageElementFinder elementFinder;

    @Override
    public void updateTaskDetails(Map<String, String> config)
    {
        getAdvancedOptionsElement().expand();

        dockerCommandField.select(Options.value(DOCKER_COMMAND_OPTION_PULL));

        if (config.containsKey(REGISTRY_OPTION))
        {
            for (PageElement radio : elementFinder.findAll(By.name(REGISTRY_OPTION)))
            {
                if (config.get(REGISTRY_OPTION).equals(radio.getValue()))
                {
                    radio.click();
                    break;
                }
            }
        }
        if (config.containsKey(REPOSITORY))
        {
            repositoryField.setText(config.get(REPOSITORY));
        }
        if (config.containsKey(CREDENTIALS_SOURCE))
        {
            if ("SHARED_CREDENTIALS".equals(config.get(CREDENTIALS_SOURCE)))
            {
                credentialsSourceIsSharedCredentials.click();
            }
        }
        if (config.containsKey(SHARED_CREDENTIALS))
        {
            sharedCredentials.select(Options.value(config.get(SHARED_CREDENTIALS)));
        }
        if (config.containsKey(USERNAME))
        {
            usernameField.setText(config.get(USERNAME));
        }
        if (config.containsKey(PASSWORD))
        {
            passwordField.setText(config.get(PASSWORD));
        }
        if (config.containsKey(EMAIL))
        {
            emailField.setText(config.get(EMAIL));
        }
        if (config.containsKey(CFG_WORKING_SUB_DIRECTORY))
        {
            workingSubDirectoryField.setText(config.get(CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(CFG_ENVIRONMENT_VARIABLES))
        {
            environmentVariablesField.setText(config.get(CFG_ENVIRONMENT_VARIABLES));
        }
    }

    private AdvancedOptionsElement getAdvancedOptionsElement()
    {
        return binder(pageBinder, AdvancedOptionsElement.class).apply(advancedOptions);
    }
}