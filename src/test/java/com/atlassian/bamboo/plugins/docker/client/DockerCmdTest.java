package com.atlassian.bamboo.plugins.docker.client;

import com.atlassian.bamboo.docker.DataVolume;
import com.atlassian.bamboo.docker.DockerContainerService;
import com.atlassian.bamboo.docker.PortMapping;
import com.atlassian.bamboo.docker.RunConfig;
import com.atlassian.bamboo.plugins.docker.process.DockerProcessService;
import com.atlassian.bamboo.plugins.docker.process.ProcessCommand;
import com.atlassian.bamboo.process.ProcessContext;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class DockerCmdTest
{
    private static final String TEST_DOCKER_PATH = "/usr/bin/docker";

    @Mock private DockerProcessService processService;
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private DockerContainerService dockerContainerService;
    @Mock private ProcessContext processContext;
    @Rule public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private Docker docker;

    @Before
    public void setUp()
    {
        this.docker = new DockerCmd(processContext, TEST_DOCKER_PATH, processService, dockerContainerService);
    }

    @Test
    @Ignore
    public void testRun() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--rm", imageName).build());
    }

    @Test
    @Ignore
    public void testRunCommand() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().command("echo \"hello world\"").build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--rm", imageName, "echo", "\"hello world\"").build());
    }

    @Test
    @Ignore
    public void testRunDetached() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().detach(true).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--detach", imageName).build());
    }

    @Test
    @Ignore
    public void testRunContainerName() throws Exception
    {
        final String imageName = "myImageName";
        final String containerName = "myContainerName";

        docker.run(imageName, RunConfig.builder().containerName(containerName).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--rm", "--name", containerName, imageName).build());
    }

    @Test
    @Ignore
    public void testRunPorts() throws Exception
    {
        final String imageName = "myImageName";
        final List<PortMapping> ports = ImmutableList.of(new PortMapping(8080, 8888), new PortMapping(5000, 5000));

        docker.run(imageName, RunConfig.builder().ports(ports).build());

        final ProcessCommand.Builder expectedCommandBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run", "--rm");
        for (PortMapping port : ports)
        {
            expectedCommandBuilder.add("-p", Joiner.on(':').join(port.getHostPort(), + port.getContainerPort()));
        }
        expectedCommandBuilder.add(imageName);

        verify(processService).execute(expectedCommandBuilder.build());
    }

    @Test
    @Ignore
    public void testRunLinks() throws Exception
    {
        final String imageName = "myImageName";
        final Map<String, String> links = ImmutableMap.of("db", "db", "webapp", "webappA");

        docker.run(imageName, RunConfig.builder().links(links).build());

        final ProcessCommand.Builder expectedCommandListBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run");
        for (Map.Entry<String, String> link : links.entrySet())
        {
            expectedCommandListBuilder.add("--link", link.getKey() + ":" + link.getValue());
        }
        expectedCommandListBuilder.add("--rm", imageName);

        verify(processService).execute(expectedCommandListBuilder.build());
    }

    @Test
    @Ignore
    public void testRunEnv() throws Exception
    {
        final String imageName = "myImageName";
        final Map<String, String> env = ImmutableMap.of("GUNICORN_OPTS", "[--preload]", "JAVA_OPTS", "-Xmx256m -Xms128m");

        docker.run(imageName, RunConfig.builder().env(env).build());

        final ProcessCommand.Builder expectedCommandListBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run", "--rm");
        for (Map.Entry<String, String> e : env.entrySet())
        {
            expectedCommandListBuilder.add("-e", e.getKey() + "=" + e.getValue());
        }
        expectedCommandListBuilder.add(imageName);

        verify(processService).execute(expectedCommandListBuilder.build());
    }

    @Test
    @Ignore
    public void testRunAdditionalArgs() throws Exception
    {
        final String imageName = "myImageName";

        docker.run(imageName, RunConfig.builder().additionalArgs("--memory=\"64m\" -c 4").build());

        final ProcessCommand.Builder expectedCommandListBuilder = ProcessCommand.builder()
                .add(TEST_DOCKER_PATH, "run", "--rm", "--memory=\"64m\"", "-c", "4", imageName);

        verify(processService).execute(expectedCommandListBuilder.build());
    }

    @Test
    @Ignore
    public void testRunVolumes() throws Exception
    {
        final String imageName = "myImageName";
        final List<DataVolume> volumes = ImmutableList.of(new DataVolume("/data", "/data"), new DataVolume("/test1", "/test2"));

        docker.run(imageName, RunConfig.builder().volumes(volumes).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--volume", "/data:/data",
                                                                    "--volume", "/test1:/test2", "--rm", imageName).build());
    }

    @Test
    @Ignore
    public void testRunWorkDir() throws Exception
    {
        final String imageName = "myImageName";
        final String workdir = "/data";

        docker.run(imageName, RunConfig.builder().workDir(workdir).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "run", "--workdir", workdir, "--rm", imageName).build());
    }

    @Test
    @Ignore
    @Parameters({"true, true", "false, false", "error, false"})
    public void testIsRunning(String output, boolean expectedResult) throws Exception
    {
        final String containerName = "myContainerName";

        when(processService.execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "inspect", "--format={{.State.Running}}", containerName).build())).thenReturn(output);

        assertThat(docker.isRunning(containerName), is(expectedResult));
    }

    @Test
    @Ignore
    public void testGetHostPort() throws Exception
    {
        final String containerName = "myContainerName";
        final Integer hostPort = 47952;
        final Integer containerPort = 5000;

        when(processService.execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "inspect",
                                                                 "--format='{{(index (index .NetworkSettings.Ports \"5000/tcp\") 0).HostPort}}'",
                                                                 containerName).build())).thenReturn(String.valueOf(hostPort));
        assertThat(docker.getHostPort(containerName, containerPort), is(hostPort));

        when(processService.execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "inspect",
                                                                 "--format='{{(index (index .NetworkSettings.Ports \"5000/tcp\") 0).HostPort}}'",
                                                                 containerName).build())).thenReturn("error");
        assertThat(docker.getHostPort(containerName, containerPort), is(nullValue()));
    }

    @Test
    @Ignore
    public void testRemove() throws Exception
    {
        final String containerName = "myContainerName";

        docker.remove(containerName);

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "rm", "-f", containerName).build());
    }

    @Test
    public void testBuild() throws Exception
    {
        final File dockerFolder = temporaryFolder.newFile("test");
        final String repository = "namespace/repository:tag";

        docker.build(dockerFolder, repository, BuildConfig.builder().build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "build", "--force-rm=true", "--tag=" + repository, dockerFolder.getAbsolutePath()).build());
    }

    @Test
    public void testBuildNoCache() throws Exception
    {
        final File dockerFolder = temporaryFolder.newFile("test");
        final String repository = "namespace/repository:tag";

        docker.build(dockerFolder, repository, BuildConfig.builder().nocache(true).build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "build", "--no-cache=true", "--force-rm=true", "--tag=" + repository, dockerFolder.getAbsolutePath()).build());
    }

    @Test
    public void testBuildWithOptions() throws Exception
    {
        final File dockerFolder = temporaryFolder.newFile("test");
        final String repository = "namespace/repository:tag";

        docker.build(dockerFolder, repository, BuildConfig.builder().additionalArguments("--label=myimage").build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "build", "--force-rm=true", "--label=myimage", "--tag=" + repository, dockerFolder.getAbsolutePath()).build());
    }

    @Test
    public void testBuildSkipTagIfOptionsDefineIt() throws Exception
    {
        final File dockerFolder = temporaryFolder.newFile("test");
        final String repository = "namespace/repository:tag";

        docker.build(dockerFolder, repository, BuildConfig.builder().additionalArguments("--tag=myimage").build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "build", "--force-rm=true", "--tag=myimage", dockerFolder.getAbsolutePath()).build());
    }

    @Test
    public void testSave() throws Exception
    {
        final String filename = "myRepository.tar";
        final String repository = "namespace/repository:tag";

        docker.save(filename, repository);

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "save", "--output=" + filename, repository).build());
    }

    @Test
    public void testTag() throws Exception
    {
        final String image = "namespace/repository:latest";
        final String repository = "namespace/repository";
        final String tag = "v15";

        docker.tag(image, repository, tag);

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "tag", image, repository + ':' + tag).build());
    }

    @Test
    public void testPush() throws Exception
    {
        final String repository = "namespace/repository";

        docker.push(repository, AuthConfig.builder().build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
    }

    @Test
    public void testPushAuth() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.push(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "--password-stdin", "-e", email).input(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());
    }

    @Test
    public void testPushAuthNoEmail() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";

        docker.push(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "--password-stdin").input(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());
    }

    @Test
    public void testPushAuthRegistry() throws Exception
    {
        final String repository = "namespace/repository";
        final String registry = "registry.druidia.com:12345";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.push(repository, AuthConfig.builder()
                .registryAddress(registry)
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "--password-stdin", "-e", email, registry).input(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout", registry).build());
    }

    @Test
    public void testPushAuthPreservesDockercfg() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";
        final String dockercfg = "ludicrous";

        final File USER_HOME = new File(System.getProperty("user.home"));
        final File DOCKERCFG = new File(USER_HOME, ".dockercfg");
        FileUtils.write(DOCKERCFG, dockercfg, Charset.defaultCharset());

        final ProcessCommand loginCommand = ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "--password-stdin", "-e", email).input(password).build();
        when(processService.executeSilently(loginCommand)).then(invocation -> {
            FileUtils.write(DOCKERCFG, "speed", Charset.defaultCharset());
            return null;
        });

        docker.push(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(loginCommand);
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "push", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());

        assertThat(FileUtils.readFileToString(DOCKERCFG, Charset.defaultCharset()), equalTo(dockercfg));
    }

    @Test
    public void testPull() throws Exception
    {
        final String repository = "namespace/repository";

        docker.pull(repository, AuthConfig.builder().build());

        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
    }

    @Test
    public void testPullAuth() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.pull(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "--password-stdin", "-e", email).input(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());
    }

    @Test
    public void testPullAuthRegistry() throws Exception
    {
        final String repository = "namespace/repository";
        final String registry = "registry.druidia.com:12345";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";

        docker.pull(repository, AuthConfig.builder()
                .registryAddress(registry)
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "--password-stdin", "-e", email, registry).input(password).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout", registry).build());
    }

    @Test
    public void testPullAuthPreservesDockercfg() throws Exception
    {
        final String repository = "namespace/repository";
        final String username = "roland";
        final String password = "12345";
        final String email = "roland@druidia.com";
        final String dockercfg = "ludicrous";

        final File USER_HOME = new File(System.getProperty("user.home"));
        final File DOCKERCFG = new File(USER_HOME, ".dockercfg");
        FileUtils.write(DOCKERCFG, dockercfg, Charset.defaultCharset());

        final ProcessCommand loginCommand = ProcessCommand.builder().add(TEST_DOCKER_PATH, "login", "-u", username, "--password-stdin", "-e", email).input(password).input(password).build();
        when(processService.executeSilently(loginCommand)).then(invocation -> {
            FileUtils.write(DOCKERCFG, "speed", Charset.defaultCharset());
            return null;
        });

        docker.pull(repository, AuthConfig.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        verify(processService).executeSilently(loginCommand);
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "pull", repository).build());
        verify(processService).execute(ProcessCommand.builder().add(TEST_DOCKER_PATH, "logout").build());

        assertThat(FileUtils.readFileToString(DOCKERCFG, Charset.defaultCharset()), equalTo(dockercfg));
    }
}
