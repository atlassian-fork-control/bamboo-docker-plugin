package com.atlassian.bamboo.plugins.docker.export;


import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.credentials.CredentialsData;
import com.atlassian.bamboo.crypto.instance.SecretEncryptionService;
import com.atlassian.bamboo.mock.MockSecretEncryptionService;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.DockerPullImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.model.task.docker.AbstractDockerTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.EMAIL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PASSWORD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_EMAIL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_PASSWORD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_REGISTRY_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_REPOSITORY;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_USERNAME;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PUSH_REPOSITORY;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REGISTRY_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REGISTRY_OPTION_CUSTOM;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REGISTRY_OPTION_HUB;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.USERNAME;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUBDIRECTORY;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RegistryTaskExporterTest
{
    private static final String SUBDIRECTORY = "/tmp/working-dir";
    private static final String ENV_VARIABLES = "-DJAVA_HOME=/opt/jdk-8";

    private static final String REPOSITORY_IMAGE = "docker-hub.org/bamboo:5.12";
    private static final String CONFIG_USER = "user";
    private static final String CONFIG_PSSWD = "password";
    private static final String CONFIG_MAIL = "user@example.com";
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    private CredentialsAccessor credentialsAccessor;
    private SecretEncryptionService secretEncryptionService;
    private RegistryTaskExporter registryTaskExporter;

    @Before
    public void setUp()
    {
        secretEncryptionService = new MockSecretEncryptionService();
        registryTaskExporter = new RegistryTaskExporter(credentialsAccessor, secretEncryptionService);
    }

    @Test
    public void testToTaskConfigFullPushConfiguration()
    {
        DockerPushImageTask task = new DockerPushImageTask()
                .customRegistryImage(REPOSITORY_IMAGE)
                .authentication(CONFIG_USER, CONFIG_PSSWD, CONFIG_MAIL)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES);

        Map<String, String> result = registryTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH),
                hasEntry(PUSH_REPOSITORY, REPOSITORY_IMAGE),
                hasEntry(USERNAME, CONFIG_USER),
                hasEntry(PASSWORD, secretEncryptionService.encrypt(CONFIG_PSSWD)),
                hasEntry(EMAIL, CONFIG_MAIL),
                hasEntry(REGISTRY_OPTION, REGISTRY_OPTION_CUSTOM),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES),
                hasEntry(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY)));
    }

    @Test
    public void testToTaskConfigFullPullConfigurationWithEmail()
    {
        DockerPullImageTask task = new DockerPullImageTask()
                .customRegistryImage(REPOSITORY_IMAGE)
                .authentication(CONFIG_USER, CONFIG_PSSWD, CONFIG_MAIL)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES);

        Map<String, String> result = registryTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL),
                hasEntry(PULL_REPOSITORY, REPOSITORY_IMAGE),
                hasEntry(PULL_USERNAME, CONFIG_USER),
                hasEntry(PULL_PASSWORD, secretEncryptionService.encrypt(CONFIG_PSSWD)),
                hasEntry(PULL_EMAIL, CONFIG_MAIL),
                hasEntry(PULL_REGISTRY_OPTION, REGISTRY_OPTION_CUSTOM),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES),
                hasEntry(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY)));
    }

    @Test
    public void testToTaskConfigFullPullConfigurationWithoutEmail()
    {
        DockerPullImageTask task = new DockerPullImageTask()
                .customRegistryImage(REPOSITORY_IMAGE)
                .authentication(CONFIG_USER, CONFIG_PSSWD)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES);

        Map<String, String> result = registryTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL),
                hasEntry(PULL_REPOSITORY, REPOSITORY_IMAGE),
                hasEntry(PULL_USERNAME, CONFIG_USER),
                hasEntry(PULL_PASSWORD, secretEncryptionService.encrypt(CONFIG_PSSWD)),
                hasEntry(PULL_EMAIL, ""),
                hasEntry(PULL_REGISTRY_OPTION, REGISTRY_OPTION_CUSTOM),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES),
                hasEntry(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY)));
    }

    @Test
    public void testToTaskConfigMinimalPushConfiguration()
    {
        DockerPushImageTask task = new DockerPushImageTask()
                .dockerHubImage(REPOSITORY_IMAGE)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES);

        Map<String, String> result = registryTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH),
                hasEntry(PUSH_REPOSITORY, REPOSITORY_IMAGE),
                hasEntry(USERNAME, ""),
                hasEntry(PASSWORD, ""),
                hasEntry(EMAIL, ""),
                hasEntry(REGISTRY_OPTION, REGISTRY_OPTION_HUB),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES),
                hasEntry(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY)));
    }

    @Test
    public void testToTaskConfigMinimalPullConfiguration()
    {
        DockerPullImageTask task = new DockerPullImageTask()
                .dockerHubImage(REPOSITORY_IMAGE)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES);

        Map<String, String> result = registryTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));

        assertThat(result, allOf(hasEntry(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL),
                hasEntry(PULL_REPOSITORY, REPOSITORY_IMAGE),
                hasEntry(PULL_USERNAME, ""),
                hasEntry(PULL_PASSWORD, ""),
                hasEntry(PULL_EMAIL, ""),
                hasEntry(PULL_REGISTRY_OPTION, REGISTRY_OPTION_HUB),
                hasEntry(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES),
                hasEntry(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToTaskConfigWrongProperties()
    {
        AnyTask task = new AnyTask(new AtlassianModule("module:key"));

        registryTaskExporter
                .toTaskConfiguration(mock(TaskContainer.class), EntityPropertiesBuilders.build(task));
    }

    @Test
    public void toSpecsEntityFullPushConfiguration()
    {
        DockerRegistryTaskProperties expectedProperties = EntityPropertiesBuilders.build(new DockerPushImageTask()
                .customRegistryImage(REPOSITORY_IMAGE)
                .authentication(CONFIG_USER, CONFIG_PSSWD, CONFIG_MAIL)
                .workingSubdirectory(SUBDIRECTORY)
                .environmentVariables(ENV_VARIABLES));


        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH);
        configuration.put(PUSH_REPOSITORY, REPOSITORY_IMAGE);
        configuration.put(REGISTRY_OPTION, REGISTRY_OPTION_CUSTOM);
        configuration.put(USERNAME, CONFIG_USER);
        configuration.put(PASSWORD, CONFIG_PSSWD);
        configuration.put(EMAIL, CONFIG_MAIL);
        configuration.put(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY);
        configuration.put(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES);

        DockerRegistryTaskProperties result = EntityPropertiesBuilders.build(registryTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));

        assertThat(result, equalTo(expectedProperties));
    }

    @Test
    public void toSpecsEntityFullPullConfiguration()
    {
        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL);
        configuration.put(PULL_REPOSITORY, REPOSITORY_IMAGE);
        configuration.put(PULL_REGISTRY_OPTION, REGISTRY_OPTION_CUSTOM);
        configuration.put(PULL_USERNAME, CONFIG_USER);
        configuration.put(PULL_PASSWORD, CONFIG_PSSWD);
        configuration.put(PULL_EMAIL, CONFIG_MAIL);
        configuration.put(CFG_ENVIRONMENT_VARIABLES, ENV_VARIABLES);
        configuration.put(CFG_WORKING_SUBDIRECTORY, SUBDIRECTORY);

        DockerRegistryTaskProperties result = EntityPropertiesBuilders.build(registryTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));

        assertThat(result.getImage(), equalTo(REPOSITORY_IMAGE));
        assertThat(result.getRegistryType(), is(DockerRegistryTaskProperties.RegistryType.CUSTOM));
        assertThat(result.getUsername(), equalTo(CONFIG_USER));
        assertThat(result.getEmail(), equalTo(CONFIG_MAIL));
        assertThat(result.getPassword(), equalTo(CONFIG_PSSWD));
        assertThat(result.getOperationType(), is(DockerRegistryTaskProperties.OperationType.PULL));
        assertThat(result.getWorkingSubdirectory(), equalTo(SUBDIRECTORY));
        assertThat(result.getEnvironmentVariables(), equalTo(ENV_VARIABLES));
    }

    @Test
    public void toSpecsEntityMinimalPushConfiguration()
    {
        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH);
        configuration.put(PUSH_REPOSITORY, REPOSITORY_IMAGE);
        configuration.put(REGISTRY_OPTION, REGISTRY_OPTION_HUB);

        DockerRegistryTaskProperties result = EntityPropertiesBuilders.build(registryTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));

        assertThat(result.getImage(), equalTo(REPOSITORY_IMAGE));
        assertThat(result.getRegistryType(), is(DockerRegistryTaskProperties.RegistryType.DOCKER_HUB));
        assertThat(result.getUsername(), isEmptyString());
        assertThat(result.getEmail(), isEmptyString());
        assertThat(result.getPassword(), isEmptyString());
        assertThat(result.getOperationType(), is(DockerRegistryTaskProperties.OperationType.PUSH));
        assertThat(result.getWorkingSubdirectory(), isEmptyString());
        assertThat(result.getEnvironmentVariables(), isEmptyString());
    }

    @Test
    public void toSpecsEntityMinimalPullConfiguration()
    {
        ImmutableMap.Builder<String, String> configuration = ImmutableMap.builder();
        configuration.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL);
        configuration.put(PULL_REPOSITORY, REPOSITORY_IMAGE);
        configuration.put(PULL_REGISTRY_OPTION, REGISTRY_OPTION_HUB);


        DockerRegistryTaskProperties result = EntityPropertiesBuilders.build(registryTaskExporter.toSpecsEntity(new TaskDefinitionImpl(0,
                AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                "",
                configuration.build())));

        assertThat(result.getImage(), equalTo(REPOSITORY_IMAGE));
        assertThat(result.getRegistryType(), is(DockerRegistryTaskProperties.RegistryType.DOCKER_HUB));
        assertThat(result.getUsername(), isEmptyString());
        assertThat(result.getEmail(), isEmptyString());
        assertThat(result.getPassword(), isEmptyString());
        assertThat(result.getOperationType(), is(DockerRegistryTaskProperties.OperationType.PULL));
        assertThat(result.getWorkingSubdirectory(), isEmptyString());
        assertThat(result.getEnvironmentVariables(), isEmptyString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void toSpecsEntityWrongTaskDefinition()
    {
        registryTaskExporter
                .toSpecsEntity(new TaskDefinitionImpl(0L, "module:key", "", Collections.emptyMap()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void toSpecsEntityWrongCommandOption()
    {
        registryTaskExporter
                .toSpecsEntity(new TaskDefinitionImpl(0L,
                        AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey(),
                        "",
                        ImmutableMap.of(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_BUILD)));
    }

    @Test
    public void testValidate()
    {
        List<ValidationProblem> result = registryTaskExporter
                .validate(mock(TaskValidationContext.class), mock(DockerRegistryTaskProperties.class));

        assertTrue(result.isEmpty());
    }

    @Test
    public void testValidateUnknownSharedCredentials()
    {
        final DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(new DockerPullImageTask()
                .authentication(new SharedCredentialsIdentifier("mycred"))
                .dockerHubImage("library/alpine"));
        List<ValidationProblem> result = registryTaskExporter.validate(mock(TaskValidationContext.class), properties);

        assertThat(result.size(), is(1));
        assertThat(result.get(0).getMessage(), containsString("Can't find shared credentials with name"));
    }

    @Test
    public void testValidateSharedCredentialsOfUnsupportedType()
    {
        final String sharedCredentialsName = "mycred";
        final DockerRegistryTaskProperties properties = EntityPropertiesBuilders.build(new DockerPullImageTask()
                .authentication(new SharedCredentialsIdentifier(sharedCredentialsName))
                .dockerHubImage("library/alpine"));
        final CredentialsData credentials = mock(CredentialsData.class);
        when(credentials.getPluginKey()).thenReturn("com.atlassian.some.plugin.key:myCredentials");
        when(credentialsAccessor.getCredentialsByName(sharedCredentialsName)).thenReturn(credentials);
        List<ValidationProblem> result = registryTaskExporter.validate(mock(TaskValidationContext.class), properties);

        assertThat(result.size(), is(1));
        assertThat(result.get(0).getMessage(), containsString("should be Username/password type"));
    }
}
