package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.export.BuildImageTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RegistryTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RunContainerTaskExporter;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPullImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(JUnitParamsRunner.class)
public class DockerServiceFactoryImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private DockerServiceFactory serviceFactory;

    @Before
    public void setUp() throws Exception {
        serviceFactory = new DockerServiceFactoryImpl(
                mock(EnvironmentVariableAccessor.class),
                mock(PollingService.class)
        );
    }

    @Test
    @Parameters(method = "dockerServiceParameters")
    public <T extends Exception> void testCreateDockerService(final String commandOption, final Class serviceClass, final Class<T> maybeException) throws Exception {
        if (maybeException != null) {
            expectedException.expect(maybeException);
        }

        DockerService dockerService = serviceFactory.create(mock(Docker.class), commandOption);

        assertThat(dockerService, instanceOf(serviceClass));
    }

    private Object dockerServiceParameters() {
        return new Object[]{
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD, BuildService.class, null},
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN, RunService.class, null},
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL, PullService.class, null},
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH, PushService.class, null},
                new Object[]{"not valid command", null, IllegalArgumentException.class},
        };
    }
}