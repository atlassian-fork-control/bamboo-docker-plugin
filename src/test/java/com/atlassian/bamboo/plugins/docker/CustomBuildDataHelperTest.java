package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableContextImpl;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertThat;

public class CustomBuildDataHelperTest
{
    @Test
    public void getDetachedContainersWhenNoContainerWasRegistered() throws Exception
    {
        final VariableContext variableContext = new VariableContextImpl(Collections.emptyMap());

        assertThat(CustomBuildDataHelper.getDetachedContainers(variableContext), emptyIterable());
    }

    @Test
    public void getDetachedContainersWhenSomeContainerWereRegistered() throws Exception
    {
        final VariableContext variableContext = new VariableContextImpl(Collections.emptyMap());

        CustomBuildDataHelper.registerContainer(variableContext, "container-1");
        CustomBuildDataHelper.registerContainer(variableContext, "container-2");

        assertThat(CustomBuildDataHelper.getDetachedContainers(variableContext), contains("container-1", "container-2"));
    }

}