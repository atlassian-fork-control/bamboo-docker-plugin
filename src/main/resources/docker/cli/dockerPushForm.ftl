[#include "../macro.ftl" /]
[@ww.radio labelKey='docker.registry' name='registryOption' toggle='true'
list='registryOptions' listKey='first' listValue='second' /]
[@s.textfield labelKey='docker.push.repository' name='pushRepository' cssClass="long-field" required=true /]
[@ui.bambooSection dependsOn='registryOption' showOn='hub']
    <div class="description">[@s.text name='docker.push.repository.hub.description' /]</div>
[/@ui.bambooSection]
[@ui.bambooSection dependsOn='registryOption' showOn='custom']
    <div class="description">[@s.text name='docker.push.repository.custom.description' /]</div>
[/@ui.bambooSection]
<br/>
[@ui.bambooSection titleKey="docker.credentials"]
    <div class="description">[@s.text name='docker.credentials.description'/]</div>
    [@s.radio name='pushCredentialsSource'
        listKey='key' listValue='value' toggle=true required=true
        list=credentialsSources cssClass='radio-group' /]
    [@ui.bambooSection dependsOn="pushCredentialsSource" showOn="USER"]
        [@s.textfield labelKey='docker.username' name='username' cssClass="long-field" required=true/]
        [#if password?has_content]
            [@s.checkbox labelKey='docker.password.change' toggle=true name='changePassword' /]
            [@ui.bambooSection dependsOn='changePassword']
                [@s.password labelKey='docker.password' name='password' cssClass="long-field" required=true/]
            [/@ui.bambooSection]
        [#else]
            [@s.hidden name='changePassword' value='true' /]
            [@s.password labelKey='docker.password' name='password' cssClass="long-field" required=true/]
        [/#if]
        [@s.textfield labelKey='docker.email' name='email' cssClass="long-field" /]
    [/@ui.bambooSection]
    [@ui.bambooSection dependsOn="pushCredentialsSource" showOn="SHARED_CREDENTIALS"]
        [#if stack.findValue("noSharedCredentials")!false]
            [@noCredentialsMessageBox id='sharedCredentials.infoBox'/]
        [#else]
            [@s.select
            id='pushSharedCredentialsId'
            labelKey='docker.task.sharedCredentials'
            name='pushSharedCredentialsId'
            list=sharedCredentials
            listKey='first'
            listValue='second']
                [@s.param name="headerKey"]-1[/@s.param]
                [@s.param name="headerValue"][@s.text name='docker.sharedCredentials.default'/][/@s.param]
            [/@s.select]
        [/#if]
    [/@ui.bambooSection]
[/@ui.bambooSection]