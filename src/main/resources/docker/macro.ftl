[#macro noCredentialsMessageBox id]
    [@ui.messageBox type='info']
    <div id="${id}">
        <p>
            [#if fn.hasRestrictedAdminPermission()]
                    [@s.text name='sharedCredentials.info.noCredentialsDefined.admin']
                        [@s.param][@s.url action='configureSharedCredentials' namespace='/admin/credentials'/][/@s.param]
                    [/@s.text]
            [#else]
                [@s.text name='sharedCredentials.info.noCredentialsDefined.nonAdmin']
                    [@s.param][@s.url action='viewAdministrators' namespace=''/][/@s.param]
                [/@s.text]
            [/#if]
        </p>
        <p>
            [@s.text name='sharedCredentials.info.noCredentialsDefined.moreInfo']
                    [@s.param][@help.href pageKey="shared.credentials"/][/@s.param]
            [/@s.text]
        </p>
    </div>
    [/@ui.messageBox]
[/#macro]