(function ($, BAMBOO) {
    BAMBOO.DOCKER = {};

    BAMBOO.DOCKER.DockerTaskConfiguration = (function () {
        var defaults = {
                pluginSelector: '#docker-plugin',
                portsSelector: null,
                volumesSelector: null,
                templates: {
                    portsListItem: null,
                    volumesListItem: null
                }
            },
            options,
            validPortRegEx = /^[0-9]{1,5}$/,
            $ports,
            $volumes,
            addPortsListItem = function () {
                var $portsList = $ports.children('tbody')
                var $portsHeader = $portsList.children(':first');
                var $lastPorts = $portsList.children(':last');
                var newIndex = ($portsList.children().length > 1 ? (parseInt($lastPorts.attr('data-ports-id'), 10) + 1) : 0);
                var $hostPort = $portsHeader.find('input[name="hostPort"]');
                var $containerPort = $portsHeader.find('input[name="containerPort"]');

                var hostPortErrorText = '';
                var containerPortErrorText = '';

                $hostPort.parent().find('.error').remove();
                $containerPort.parent().find('.error').remove();

                var portList = $ports.find('input').serializeArray().filter(function (field) {
                    return (field.name && (field.name.indexOf('containerPort_') === 0 || field.name.indexOf('hostPort_') === 0));
                });
                var hasDuplicateHostPort = portList.some(function (field) {
                    return (field.name.indexOf('hostPort') === 0 && $hostPort.val() === field.value);
                });
                var hasDuplicateContainerPort = portList.some(function (field) {
                    return (field.name.indexOf('containerPort') === 0 && $containerPort.val() === field.value);
                });

                if ($hostPort.val()) {
                    if (!validPortRegEx.test($hostPort.val())) {
                        hostPortErrorText = AJS.I18n.getText('docker.ports.error.invalid');
                    } else if (hasDuplicateHostPort) {
                        hostPortErrorText = AJS.I18n.getText('docker.ports.error.duplicate');
                    }
                }

                if (!$containerPort.val()) {
                    containerPortErrorText = AJS.I18n.getText('docker.ports.container.error.empty');
                } else if (!validPortRegEx.test($containerPort.val())) {
                    containerPortErrorText = AJS.I18n.getText('docker.ports.error.invalid');
                } else if (hasDuplicateContainerPort) {
                    containerPortErrorText = AJS.I18n.getText('docker.ports.error.duplicate');
                }

                if (hostPortErrorText || containerPortErrorText) {
                    $hostPort.after($('<div class="error control-form-error"/>').text(hostPortErrorText));
                    $containerPort.after($('<div class="error control-form-error"/>').text(containerPortErrorText));
                    return;
                }

                $(AJS.template.load(options.templates.portsListItem).fill({
                    index: newIndex,
                    hostPort: $hostPort.val(),
                    containerPort: $containerPort.val()
                }).toString()).hide().appendTo($portsList).end().slideDown();

                $hostPort.val('');
                $containerPort.val('');

                BAMBOO.DynamicFieldParameters.syncFieldShowHide($portsList);
            },
            removePortsListItem = function () {
                $(this).closest('tr').slideUp(function () { $(this).remove(); });
            },
            onFocusPortField = function (evt) {
                $(this).parents('tr').addClass('active');
            },
            onBlurPortField = function (evt) {
                $(this).parents('tr').removeClass('active');
            },
            validatePortMappings = function (event) {
                $ports.find('.error.control-form-error').remove();
                var portList = $ports.find('input').serializeArray().filter(function (field) {
                    return (field.name && (field.name.indexOf('containerPort_') === 0 || field.name.indexOf('hostPort_') === 0));
                });
                portList.forEach(function (field) {
                    var errorText = '';
                    var hasDuplicatePort = portList.some(function (f) {
                        return (f.name.indexOf('containerPort') === 0 && field.name.indexOf('containerPort') === 0 && f.value === field.value && field.name !== f.name)
                            || (f.name.indexOf('hostPort') === 0 && field.name.indexOf('hostPort') === 0 && f.value === field.value && field.name !== f.name);
                    });
                    if (!field.value) {
                        if (field.name.indexOf('containerPort') === 0) {
                            errorText = AJS.I18n.getText('docker.ports.container.error.empty');
                        }
                    } else if (!validPortRegEx.test(field.value)) {
                        errorText = AJS.I18n.getText('docker.ports.error.invalid');
                    } else if (hasDuplicatePort) {
                        errorText = AJS.I18n.getText('docker.ports.error.duplicate');
                    }
                    if (errorText) {
                        $('#' + field.name).closest('td').append($('<div/>').addClass('error control-form-error').text(errorText));
                        event.stopPropagation();
                        event.preventDefault();
                    }
                });
            },
            addVolumesListItem = function () {
                var $volumesList = $volumes.children('tbody')
                var $volumesHeader = $volumesList.children(':first');
                var $lastVolumes = $volumesList.children(':last');
                var newIndex = ($volumesList.children().length > 1 ? (parseInt($lastVolumes.attr('data-volumes-id'), 10) + 1) : 0);
                var $hostDirectory = $volumesHeader.find('input[name="hostDirectory"]');
                var $containerDataVolume = $volumesHeader.find('input[name="containerDataVolume"]');

                $containerDataVolume.parent().find('.error').remove();
                if (!$containerDataVolume.val()) {
                    return $containerDataVolume.after($('<div class="error control-form-error"/>').text(AJS.I18n.getText('docker.volumes.containerDataVolume.error.empty')));
                }

                $(AJS.template.load(options.templates.volumesListItem).fill({
                    index: newIndex,
                    hostDirectory: $hostDirectory.val(),
                    containerDataVolume: $containerDataVolume.val()
                }).toString()).hide().appendTo($volumesList).end().slideDown();

                $hostDirectory.val('');
                $containerDataVolume.val('');

                BAMBOO.DynamicFieldParameters.syncFieldShowHide($volumesList);
            },
            removeVolumesListItem = function () {
                $(this).closest('tr').slideUp(function () { $(this).remove(); });
            },
            onFocusVolumeField = function (evt){
                $(this).parents('tr').addClass('active');
            },
            onBlurVolumeField = function (evt){
                $(this).parents('tr').removeClass('active');
            },
            onInputVolumeContainer = function (evt){
                if (!$(this).val()) {
                    $(this).parents('tr').addClass('error');
                    $(this).after($('<div class="error control-form-error"/>').text(AJS.I18n.getText('docker.volumes.containerDataVolume.error.empty')));
                } else {
                    $(this).parents('tr').removeClass('error');
                    $(this).parent().find('.error').remove();
                }
            };

        return {
            init: function (opts) {
                options = $.extend(true, defaults, opts);

                $(function () {
                    $ports = $(options.portsSelector)
                        .on('focus', 'input[type="text"]', onFocusPortField)
                        .on('blur', 'input[type="text"]', onBlurPortField)
                        .on('input', options.portsSelector + '-host--edit input[type="text"]', validatePortMappings)
                        .on('input', options.portsSelector + '-container--edit input[type="text"]', validatePortMappings)
                        .on('click', options.portsSelector + '-actions--add', addPortsListItem)
                        .on('click', options.portsSelector + '-actions--remove', removePortsListItem);
                    $ports.find('.error').parents('tr').addClass('error');
                    $(options.pluginSelector).closest('form').submit(function () {
                        return $ports.find(options.portsSelector + '-container--edit.error').length;
                    });

                    $volumes = $(options.volumesSelector)
                        .on('focus', 'input[type="text"]', onFocusVolumeField)
                        .on('blur',  'input[type="text"]', onBlurVolumeField)
                        .on('input', options.volumesSelector + '-container--edit input[type="text"]', onInputVolumeContainer)
                        .on('click', options.volumesSelector + '-actions--add', addVolumesListItem)                    
                        .on('click', options.volumesSelector + '-actions--remove', removeVolumesListItem);
                    $volumes.find('.error').parents('tr').addClass('error');
                    $(options.pluginSelector).closest('form').submit(function() {
                        return $volumes.find(options.volumesSelector + '-container--edit.error').length;
                    });
                });
            }
        };
    }());
}(jQuery, window.BAMBOO = (window.BAMBOO || {})));