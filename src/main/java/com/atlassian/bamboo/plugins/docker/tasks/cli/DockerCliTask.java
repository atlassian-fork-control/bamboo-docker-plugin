package com.atlassian.bamboo.plugins.docker.tasks.cli;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.docker.DockerContainerService;
import com.atlassian.bamboo.plugins.docker.DockerCapabilityTypeModule;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.client.DockerCmd;
import com.atlassian.bamboo.plugins.docker.process.DockerTaskProcessService;
import com.atlassian.bamboo.plugins.docker.service.DockerService;
import com.atlassian.bamboo.plugins.docker.service.DockerServiceFactory;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.process.ProcessContextFactory;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

public class DockerCliTask implements CommonTaskType
{
    private final ProcessService processService;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final CapabilityContext capabilityContext;
    private final DockerServiceFactory serviceFactory;
    private final DockerContainerService dockerContainerService;
    private final ProcessContextFactory processContextFactory;

    @Autowired
    public DockerCliTask(@ComponentImport final ProcessService processService,
                         @ComponentImport final EnvironmentVariableAccessor environmentVariableAccessor,
                         @ComponentImport final CapabilityContext capabilityContext,
                         @ComponentImport final DockerContainerService dockerContainerService,
                         @ComponentImport final ProcessContextFactory processContextFactory,
                         final DockerServiceFactory serviceFactory)
    {
        this.processService = processService;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.capabilityContext = capabilityContext;
        this.serviceFactory = serviceFactory;
        this.dockerContainerService = dockerContainerService;
        this.processContextFactory = processContextFactory;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

        final String dockerPath = capabilityContext.getCapabilityValue(DockerCapabilityTypeModule.DOCKER_CAPABILITY);
        final Map<String, String> extraEnvironmentVariables = environmentVariableAccessor.splitEnvironmentAssignments(
                configurationMap.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), false);
        final TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);
        final Docker docker = new DockerCmd(processContextFactory.forTaskContext(taskContext),
                dockerPath,
                DockerTaskProcessService.builder(taskContext, taskResultBuilder, processService)
                .environmentVariables(extraEnvironmentVariables).build(), dockerContainerService);
        taskContext.getWorkingDirectory().mkdirs();
        final String dockerCommandOption = configurationMap.get(DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION);
        final DockerService dockerService = serviceFactory.create(docker, dockerCommandOption);

        dockerService.execute(taskContext);

        return taskResultBuilder.build();
    }
}