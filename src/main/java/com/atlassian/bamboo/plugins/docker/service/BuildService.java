package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plugins.docker.RepositoryKey;
import com.atlassian.bamboo.plugins.docker.RepositoryKeys;
import com.atlassian.bamboo.plugins.docker.client.BuildConfig;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.config.BuildConfiguration;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.google.common.io.Files;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class BuildService implements DockerService
{
    private final Docker docker;

    public BuildService(Docker docker)
    {
        this.docker = docker;
    }

    @Override
    public void execute(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final BuildLogger logger = taskContext.getBuildLogger();
        final BuildConfiguration buildConfiguration = BuildConfiguration.fromContext(taskContext);

        if (buildConfiguration.isDockerfileInline())
        {
            writeDockerfile(buildConfiguration);
        }

        try
        {
            final BuildConfig buildConfig = BuildConfig.builder()
                    .nocache(buildConfiguration.isNocache())
                    .additionalArguments(buildConfiguration.getAdditionalArguments())
                    .build();
            docker.build(taskContext.getWorkingDirectory(), buildConfiguration.getRepository(), buildConfig);
            logger.addBuildLogEntry(String.format("Built Docker image '%s'", buildConfiguration.getRepository()));

            if (buildConfiguration.isSave())
            {
                final RepositoryKey repositoryKey = RepositoryKeys.parseKey(buildConfiguration.getRepository());
                docker.save(buildConfiguration.getFilename(), repositoryKey.getEncodedRepositoryString());
                logger.addBuildLogEntry(String.format("Saved Docker image '%s' to file '%s'", buildConfiguration.getRepository(),
                                                      new File(taskContext.getWorkingDirectory(), buildConfiguration.getFilename()).getAbsolutePath()));
            }
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }

    private void writeDockerfile(@NotNull final BuildConfiguration config) throws TaskException
    {
        try
        {
            File dockerFile = new File(config.getWorkingDirectory(), "Dockerfile");
            Files.write(config.getDockerfile(), dockerFile, StandardCharsets.UTF_8);
        }
        catch (IOException e)
        {
            throw new TaskException("Unable to create Dockerfile", e);
        }
    }
}