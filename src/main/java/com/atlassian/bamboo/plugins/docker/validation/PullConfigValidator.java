package com.atlassian.bamboo.plugins.docker.validation;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.docker.RepositoryKey;
import com.atlassian.bamboo.plugins.docker.RepositoryKeys;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class PullConfigValidator implements ConfigValidator
{
    private I18nResolver i18nResolver;

    public PullConfigValidator(I18nResolver i18nResolver)
    {
        this.i18nResolver = i18nResolver;
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection)
    {
        final RepositoryKey repositoryKey = RepositoryKeys.parseKey(params.getString(DockerCliTaskConfigurator.PULL_REPOSITORY));

        if (DockerCliTaskConfigurator.REGISTRY_OPTION_HUB.equals(params.getString(DockerCliTaskConfigurator.PULL_REGISTRY_OPTION)))
        {
            if (repositoryKey.getRegistry() != null)
            {
                errorCollection.addError(DockerCliTaskConfigurator.PULL_REPOSITORY, i18nResolver.getText("docker.pull.repository.registry.error.notEmpty"));
            }
        }
        else if (DockerCliTaskConfigurator.REGISTRY_OPTION_CUSTOM.equals(params.getString(DockerCliTaskConfigurator.PULL_REGISTRY_OPTION)))
        {
            if (repositoryKey.getRegistry() == null)
            {
                errorCollection.addError(DockerCliTaskConfigurator.PULL_REPOSITORY, i18nResolver.getText("docker.pull.repository.registry.error.empty"));
            }
        }

        if (StringUtils.isBlank(repositoryKey.getRepository()))
        {
            errorCollection.addError(DockerCliTaskConfigurator.PULL_REPOSITORY, i18nResolver.getText("docker.repository.error.empty"));
        }
        final String credentialsSource = params.getString(DockerCliTaskConfigurator.PULL_CREDENTIALS_SOURCE, "");
        if (DockerCliTaskConfigurator.CREDENTIALS_SOURCE_USER.equals(credentialsSource))
        {
            if (StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.PULL_USERNAME)))
            {
                errorCollection.addError(DockerCliTaskConfigurator.PULL_USERNAME, i18nResolver.getText("docker.username.error.empty"));
            }
            if (params.getBoolean(DockerCliTaskConfigurator.PULL_CHANGE_PASSWORD) && StringUtils.isBlank(params.getString(DockerCliTaskConfigurator.PULL_PASSWORD)))
            {
                errorCollection.addError(DockerCliTaskConfigurator.PULL_PASSWORD, i18nResolver.getText("docker.password.error.empty"));
            }
        }
        else if (DockerCliTaskConfigurator.CREDENTIALS_SOURCE_SHARED_CREDENTIALS.equals(credentialsSource))
        {
            if (params.getLong(DockerCliTaskConfigurator.PULL_SHARED_CREDENTIALS_ID, -1) == -1)
            {
                errorCollection.addError(DockerCliTaskConfigurator.PULL_SHARED_CREDENTIALS_ID, i18nResolver.getText("docker.shared_credentials.error.required"));
            }
        }
    }
}