package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.credentials.CredentialsData;
import com.atlassian.bamboo.credentials.UsernamePasswordCredentialType;
import com.atlassian.bamboo.crypto.instance.SecretEncryptionService;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.RuntimeTaskDataProvider;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import com.atlassian.bamboo.v2.build.CommonContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.bamboo.credentials.UsernamePasswordCredentialType.CFG_PASSWORD;
import static com.atlassian.bamboo.credentials.UsernamePasswordCredentialType.CFG_USERNAME;

public class DockerRuntimeDataProvider implements RuntimeTaskDataProvider
{
    @Inject
    private CredentialsAccessor credentialsAccessor;
    @Inject
    private SecretEncryptionService secretEncryptionService;

    @NotNull
    @Override
    public Map<String, String> populateRuntimeTaskData(@NotNull TaskDefinition taskDefinition, @NotNull CommonContext commonContext)
    {
        final Map<String, String> configuration = taskDefinition.getConfiguration();
        final Map<String, String> result = new HashMap<>();
        propagateSharedCredentialsValues(DockerCliTaskConfigurator.PUSH_SHARED_CREDENTIALS_ID, DockerCliTaskConfigurator.USERNAME, DockerCliTaskConfigurator.PASSWORD, taskDefinition, configuration, result);
        propagateSharedCredentialsValues(DockerCliTaskConfigurator.PULL_SHARED_CREDENTIALS_ID, DockerCliTaskConfigurator.PULL_USERNAME, DockerCliTaskConfigurator.PULL_PASSWORD, taskDefinition, configuration, result);
        return result;
    }

    private void propagateSharedCredentialsValues(String sharedCredentialsKey, String usernameKey, String passwordKey, @NotNull TaskDefinition taskDefinition, Map<String, String> configuration, Map<String, String> result)
    {
        final String credentialsId = configuration.get(sharedCredentialsKey);
        if (StringUtils.isNotBlank(credentialsId) && !"-1".equals(credentialsId))
        {
            final CredentialsData credentials
                    = credentialsAccessor.getCredentials(Long.parseLong(credentialsId));
            if (credentials == null)
            {
                throw new IllegalStateException("Can't find shared credentials with id " + credentialsId
                        + " for task "
                        + (StringUtils.isEmpty(taskDefinition.getUserDescription()) ? taskDefinition.getPluginKey() : taskDefinition.getUserDescription()));
            }

            if (credentials.getPluginKey().equals(UsernamePasswordCredentialType.PLUGIN_KEY))
            { //password
                final String username = credentials.getConfiguration().get(CFG_USERNAME);
                final String password = credentials.getConfiguration().get(CFG_PASSWORD);
                result.put(usernameKey, username);
                result.put(passwordKey, password);
            }
        }
        else
        {
            result.put(usernameKey, configuration.get(usernameKey));
            final String password = configuration.get(passwordKey);
            if(secretEncryptionService.isEncrypted(password)) {
                result.put(passwordKey, secretEncryptionService.decrypt(password));
            } else {
                result.put(passwordKey, password);
            }
        }
    }

    @Override
    public void processRuntimeTaskData(@NotNull RuntimeTaskDefinition runtimeTaskDefinition, @NotNull CommonContext commonContext)
    {
    }
}
