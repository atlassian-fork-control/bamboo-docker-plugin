package com.atlassian.bamboo.plugins.docker.process;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.logger.LogInterceptor;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskState;
import com.atlassian.bamboo.util.PasswordMaskingUtils;
import com.atlassian.utils.process.ProcessException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class DockerTaskProcessService implements DockerProcessService
{
    private final CommonTaskContext taskContext;
    private final TaskResultBuilder taskResultBuilder;
    private final ProcessService processService;
    private final ImmutableMap<String, String> environmentVariables;
    private final DockerProcessService defaultProcessService;

    private DockerTaskProcessService(@NotNull final Builder builder)
    {
        this.taskContext = builder.taskContext;
        this.taskResultBuilder = builder.taskResultBuilder;
        this.processService = builder.processService;
        this.environmentVariables = ImmutableMap.copyOf(builder.environmentVariables);

        this.defaultProcessService = DefaultDockerProcessService.builder()
                .environmentVariables(builder.environmentVariables)
                .workingDir(taskContext.getWorkingDirectory())
                .build();
    }

    @NotNull
    public String execute(@NotNull final ProcessCommand command) throws ProcessException
    {
        final BuildLogInterceptor interceptor = new BuildLogInterceptor();

        try
        {
            taskContext.getBuildLogger().getInterceptorStack().add(interceptor);
            taskContext.getWorkingDirectory().mkdirs();
            final ExternalProcessBuilder externalProcessBuilder = new ExternalProcessBuilder()
                    .command(command.getCommandList())
                    .workingDirectory(taskContext.getWorkingDirectory())
                    .input(command.getInput());

            externalProcessBuilder.env(environmentVariables);

            taskResultBuilder.checkReturnCode(processService.executeExternalProcess(taskContext, externalProcessBuilder));

            if (taskResultBuilder.getTaskState() != TaskState.SUCCESS)
            {
                throw new ProcessException("Error executing " + PasswordMaskingUtils.maskPossiblePasswordValues(command.getSafeCommandString(), taskContext.getCommonContext().getVariableContext()));
            }
        }
        finally
        {
            taskContext.getBuildLogger().getInterceptorStack().remove(interceptor);
        }

        return interceptor.getOutput();
    }

    @NotNull
    @Override
    public String executeSilently(@NotNull ProcessCommand command) throws ProcessException
    {
        return defaultProcessService.executeSilently(command);
    }

    @Override
    public Path getWorkingDirectory() {
       return taskContext.getWorkingDirectory() != null ? taskContext.getWorkingDirectory().toPath() : null;
    }

    @Override
    public Map<String, String> getEnvironmentVariables() {
        return environmentVariables;
    }

    public static Builder builder(@NotNull final CommonTaskContext taskContext, @NotNull final TaskResultBuilder taskResultBuilder,
                                  @NotNull final ProcessService processService)
    {
        return new Builder(taskContext, taskResultBuilder, processService);
    }

    public static class Builder
    {
        private final CommonTaskContext taskContext;
        private final TaskResultBuilder taskResultBuilder;
        private final ProcessService processService;
        private Map<String, String> environmentVariables = Collections.emptyMap();

        private Builder(@NotNull final CommonTaskContext taskContext, @NotNull final TaskResultBuilder taskResultBuilder,
                        @NotNull final ProcessService processService)
        {
            this.taskContext = taskContext;
            this.taskResultBuilder = taskResultBuilder;
            this.processService = processService;
        }

        public Builder environmentVariables(@NotNull final Map<String, String> environmentVariables)
        {
            this.environmentVariables = environmentVariables;
            return this;
        }

        @NotNull
        public DockerTaskProcessService build()
        {
            return new DockerTaskProcessService(this);
        }
    }

    private class BuildLogInterceptor implements LogInterceptor
    {
        private final List<String> output = new ArrayList<>();

        @Override
        public void intercept(@NotNull LogEntry logEntry)
        {
            output.add(logEntry.getLog());
        }

        @Override
        public void interceptError(@NotNull LogEntry logEntry)
        {

        }

        @NotNull
        public String getOutput()
        {
            return Iterables.getLast(output);
        }
    }
}
