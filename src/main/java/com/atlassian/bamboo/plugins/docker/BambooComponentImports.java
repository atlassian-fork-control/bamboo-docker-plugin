package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.crypto.instance.SecretEncryptionService;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.struts.TextProvider;
import org.jetbrains.annotations.NotNull;

/**
 * References a OSGI beans which are exported by Bamboo Server and used by this plugin on Bamboo Server
 * side (not on Agents)
 */
@SuppressWarnings("unused")
public class BambooComponentImports
{
    @BambooImport
    private TemplateRenderer templateRenderer;
    @BambooImport
    private TextProvider textProvider;
    @BambooImport
    private TaskConfiguratorHelper taskConfiguratorHelper;
    @BambooImport
    private SecretEncryptionService secretEncryptionService;
    @BambooImport
    private CredentialsAccessor credentialsAccessor;
    @BambooImport
    private CachedPlanManager cachedPlanManager;
    @BambooImport
    private TaskConfigurationService taskConfigurationService;
    @BambooImport
    private EnvironmentService environmentService;
    @BambooImport
    private EnvironmentTaskService environmentTaskService;
    @BambooImport
    private BuildDefinitionManager buildDefinitionManager;
    @BambooImport
    private DeploymentProjectService deploymentProjectService;
    @BambooImport
    private PlanManager planManager;
}
