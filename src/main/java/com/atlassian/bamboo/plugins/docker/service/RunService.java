package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.docker.DockerException;
import com.atlassian.bamboo.docker.PortMapping;
import com.atlassian.bamboo.docker.RunConfig;
import com.atlassian.bamboo.plugins.docker.CustomBuildDataHelper;
import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.config.RunConfiguration;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.plugins.docker.utils.DockerEnvironmentVariablesHelper;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.utils.Pair;
import com.atlassian.bamboo.variable.VariableContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RunService implements DockerService
{
    private static final Logger log = Logger.getLogger(RunService.class);

    private final Docker docker;
    private final EnvironmentVariableAccessor environmentVariableAccessor;
    private final PollingService pollingService;

    public RunService(@NotNull final Docker docker, @NotNull final EnvironmentVariableAccessor environmentVariableAccessor,
                      @NotNull final PollingService pollingService)
    {
        this.docker = docker;
        this.environmentVariableAccessor = environmentVariableAccessor;
        this.pollingService = pollingService;
    }

    @Override
    public void execute(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final BuildLogger logger = taskContext.getBuildLogger();
        final Map<String, String> customData = taskContext.getCommonContext().getCurrentResult().getCustomBuildData();
        final VariableContext variableContext = taskContext.getCommonContext().getVariableContext();
        final RunConfiguration runConfiguration = RunConfiguration.fromContext(taskContext);

        try
        {
            final RunConfig runConfig = buildRunConfig(runConfiguration, variableContext);

            // The container name should be registered before the run command so that it is still removed if the run errors.
            if (runConfiguration.isRunDetached())
            {
                log.debug("Registering container: " + runConfiguration.getName());
                CustomBuildDataHelper.registerContainer(variableContext, runConfiguration.getName());
            }

            logger.addBuildLogEntry(String.format("Running image name (%s)", runConfiguration.getImage()));
            docker.run(runConfiguration.getImage(), runConfig);

            if (runConfiguration.isWaitForService())
            {
                final String serviceUrl = getServiceUrl(runConfiguration);
                pollingService.waitUntilAvailable(serviceUrl, runConfiguration.getServiceTimeout(), logger);
                CustomBuildDataHelper.setDeployedServiceUrl(customData, serviceUrl);
            }
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }

    @NotNull
    private RunConfig buildRunConfig(@NotNull final RunConfiguration config, @NotNull final VariableContext variableContext) throws DockerException
    {
        final Map<String, String> links = getLinks(config, variableContext);

        final Pair<Map<String, String>, List<String>> extraEnvironmentVariables = DockerEnvironmentVariablesHelper.splitEnvironmentVariables(
                environmentVariableAccessor, config.getEnvironmentVariables());

        final RunConfig.Builder runConfig = RunConfig.builder()
                .ports(config.getPorts())
                .volumes(config.getVolumes())
                .detach(config.isRunDetached())
                .links(links)
                .env(extraEnvironmentVariables.first)
                .passedEnv(extraEnvironmentVariables.second);

        if (StringUtils.isNotBlank(config.getName()))
        {
            runConfig.containerName(config.getName());
        }

        if (StringUtils.isNotBlank(config.getCommand()))
        {
            runConfig.command(config.getCommand());
        }

        if (StringUtils.isNotBlank(config.getWorkDir()))
        {
            runConfig.workDir(config.getWorkDir());
        }

        if (StringUtils.isNotBlank(config.getAdditionalArgs()))
        {
            runConfig.additionalArgs(config.getAdditionalArgs());
        }

        return runConfig.build();
    }

    @NotNull
    private Map<String, String> getLinks(@NotNull final RunConfiguration taskConfig, @NotNull final VariableContext variableContext) throws DockerException
    {
        final Map<String, String> links = new HashMap<>();

        if (taskConfig.isLink())
        {
            final Iterable<String> detachedContainers = CustomBuildDataHelper.getDetachedContainers(variableContext);

            for (String detachedContainerName : detachedContainers)
            {
                if (docker.isRunning(detachedContainerName))
                {
                    links.put(detachedContainerName, detachedContainerName);
                }
            }
        }

        return links;
    }

    @Nullable
    private String getServiceUrl(@NotNull final RunConfiguration runConfig) throws DockerException
    {
        final String serviceUrlPattern = runConfig.getServiceUrl();
        final PortMapping firstPortMapping = runConfig.getFirstPort();

        if (StringUtils.isBlank(serviceUrlPattern) || firstPortMapping == null)
        {
            return serviceUrlPattern;
        }

        final Integer hostPort;
        final Integer hostPortFromConfiguration = firstPortMapping.getHostPort();
        if (hostPortFromConfiguration != null)
        {
            hostPort = hostPortFromConfiguration;
        }
        else
        {
            // this actually runs docker command
            hostPort = docker.getHostPort(runConfig.getName(), firstPortMapping.getContainerPort());
        }

        return StringUtils.replace(serviceUrlPattern, DockerCliTaskConfigurator.SERVICE_URL_PORT_PLACEHOLDER, String.valueOf(hostPort));
    }
}
