package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.RepositoryKey;
import com.atlassian.bamboo.plugins.docker.RepositoryKeys;
import com.atlassian.bamboo.plugins.docker.client.AuthConfig;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.config.PullConfiguration;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import org.jetbrains.annotations.NotNull;

public class PullService implements DockerService
{
    private final Docker docker;

    public PullService(Docker docker)
    {
        this.docker = docker;
    }

    @Override
    public void execute(@NotNull final CommonTaskContext taskContext) throws TaskException
    {
        final PullConfiguration pullConfig = PullConfiguration.fromContext(taskContext);

        try 
        {
            final RepositoryKey repositoryKey = RepositoryKeys.parseKey(pullConfig.getRepository());

            final AuthConfig authConfig = AuthConfig.builder()
                    .registryAddress(repositoryKey.getRegistry())
                    .username(pullConfig.getUsername())
                    .password(pullConfig.getPassword())
                    .email(pullConfig.getEmail())
                    .build();

            docker.pull(repositoryKey.getEncodedRepositoryString(), authConfig);
        }
        catch (Exception e)
        {
            throw new TaskException("Failed to execute task", e);
        }
    }
}