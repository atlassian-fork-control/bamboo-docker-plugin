package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Map;

public abstract class CustomBuildDataHelper
{
    private static final String DEPLOYED_SERVICE_URL = "deployedServiceUrl";
    private static final String RUN_CONTAINER_NAMES = "dockerContainerNames";

    public static void setDeployedServiceUrl(@NotNull final Map<String, String> customData, @NotNull String deployedServiceUrl)
    {
        customData.put(DEPLOYED_SERVICE_URL, deployedServiceUrl);
    }

    @Nullable
    public String getDeployedServiceURL(@NotNull final Map<String, String> customData)
    {
        return customData.get(DEPLOYED_SERVICE_URL);
    }

    public static void registerContainer(@NotNull final VariableContext variableContext, @NotNull String name)
    {
        final Map<String, VariableDefinitionContext> effectiveVariables = variableContext.getEffectiveVariables();
        final VariableDefinitionContext variableDefinitionContext = effectiveVariables.get(RUN_CONTAINER_NAMES);
        final String currentRunContainerNames = variableDefinitionContext != null ? variableDefinitionContext.getValue() : "";
        final String newRunContainerNames = StringUtils.join(currentRunContainerNames, ' ', name).trim();
        variableContext.addLocalVariable(RUN_CONTAINER_NAMES, newRunContainerNames);
    }

    @NotNull
    public static Iterable<String> getDetachedContainers(@NotNull final VariableContext variableContext)
    {
        final Map<String, VariableDefinitionContext> effectiveVariables = variableContext.getEffectiveVariables();
        if (effectiveVariables.containsKey(RUN_CONTAINER_NAMES))
        {
            final String containerNames = effectiveVariables.get(RUN_CONTAINER_NAMES).getValue();
            if (StringUtils.isNotBlank(containerNames))
            {
                return Splitter.on(' ').split(containerNames);
            }
        }

        return Collections.emptySet();
    }
}
