package com.atlassian.bamboo.plugins.docker;

import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.docker.DockerContainerService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.client.DockerCmd;
import com.atlassian.bamboo.plugins.docker.process.DefaultDockerProcessService;
import com.atlassian.bamboo.process.ProcessContextFactory;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.BuildContextHelper;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilityContext;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A custom build processor that cleans up Docker containers at the end of the build.
 */
public class DockerContainerCleanupProcessor implements CustomBuildProcessor {
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(DockerContainerCleanupProcessor.class);

    private final DockerContainerService dockerContainerService;

    private BuildContext buildContext;
    private final CapabilityContext capabilityContext;
    private final ProcessContextFactory processContextFactory;

    @Autowired
    public DockerContainerCleanupProcessor(@ComponentImport DockerContainerService dockerContainerService,
                                           @ComponentImport CapabilityContext capabilityContext,
                                           @ComponentImport ProcessContextFactory processContextFactory) {
        this.dockerContainerService = dockerContainerService;
        this.capabilityContext = capabilityContext;
        this.processContextFactory = processContextFactory;
    }

    @Override
    public void init(@NotNull BuildContext buildContext) {
        this.buildContext = buildContext;
    }

    @NotNull
    @Override
    public BuildContext call() throws Exception {
        final String dockerPath = capabilityContext.getCapabilityValue(DockerCapabilityTypeModule.DOCKER_CAPABILITY);
        final Docker docker = new DockerCmd(processContextFactory.forCommonContext(buildContext),
                dockerPath, DefaultDockerProcessService.builder().workingDir(BuildContextHelper.getBuildWorkingDirectory(buildContext)).build(), dockerContainerService);

        final Iterable<String> detachedContainers = CustomBuildDataHelper.getDetachedContainers(buildContext.getVariableContext());

        for (String detachedContainer : detachedContainers) {
            log.debug("Removing container: " + detachedContainer);
            docker.remove(detachedContainer);
        }

        return buildContext;
    }
}
