package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import org.jetbrains.annotations.NotNull;

public interface DockerExporterFactory {
    @NotNull
    TaskDefinitionExporter createExporter(@NotNull final String dockerCommandOption) throws IllegalArgumentException;

    @NotNull
    TaskDefinitionExporter createExporter(@NotNull final TaskProperties taskProperties) throws IllegalArgumentException;
}
