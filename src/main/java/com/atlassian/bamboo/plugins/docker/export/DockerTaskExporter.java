package com.atlassian.bamboo.plugins.docker.export;


import com.atlassian.bamboo.plugins.docker.service.DockerExporterFactory;
import com.atlassian.bamboo.plugins.docker.service.DockerServiceFactory;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

public class DockerTaskExporter implements TaskDefinitionExporter {

    private static final Logger log = Logger.getLogger(DockerTaskExporter.class);

    private final DockerExporterFactory dockerExporterFactory;

    @Inject
    public DockerTaskExporter(DockerExporterFactory dockerExporterFactory) {
        this.dockerExporterFactory = dockerExporterFactory;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull TaskProperties taskProperties) {
        return dockerExporterFactory.createExporter(taskProperties).toTaskConfiguration(taskContainer, taskProperties);
    }

    @NotNull
    @Override
    public Task toSpecsEntity(@NotNull TaskDefinition taskDefinition) {
        log.debug(String.format("Exporting task definition by id: %s and plugin key: %s", taskDefinition.getId(), taskDefinition.getPluginKey()));
        String commandOption = taskDefinition.getConfiguration().getOrDefault(DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION, "");
        return dockerExporterFactory
                .createExporter(commandOption)
                .toSpecsEntity(taskDefinition);
    }

    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        try {
            return dockerExporterFactory
                    .createExporter(taskProperties)
                    .validate(taskValidationContext, taskProperties);
        } catch (IllegalArgumentException ex) {
            return Lists.newArrayList();
        }
    }
}
