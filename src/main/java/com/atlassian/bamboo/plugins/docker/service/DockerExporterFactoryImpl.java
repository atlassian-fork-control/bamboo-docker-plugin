package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.export.BuildImageTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RegistryTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RunContainerTaskExporter;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRunContainerTaskProperties;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;

@BambooComponent
public class DockerExporterFactoryImpl implements DockerExporterFactory {
    private final BuildImageTaskExporter buildImageTaskExporter;
    private final RegistryTaskExporter registryTaskExporter;
    private final RunContainerTaskExporter runContainerTaskExporter;

    @Inject
    public DockerExporterFactoryImpl(final BuildImageTaskExporter buildImageTaskExporter,
                                     final RegistryTaskExporter registryTaskExporter,
                                     final RunContainerTaskExporter runContainerTaskExporter) {
        this.buildImageTaskExporter = buildImageTaskExporter;
        this.registryTaskExporter = registryTaskExporter;
        this.runContainerTaskExporter = runContainerTaskExporter;
    }

    @NotNull
    @Override
    public TaskDefinitionExporter createExporter(@NotNull String dockerCommandOption) {
        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD.equals(dockerCommandOption)) {
            return buildImageTaskExporter;
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN.equals(dockerCommandOption)) {
            return runContainerTaskExporter;
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH.equals(dockerCommandOption)) {
            return registryTaskExporter;
        }

        if (DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL.equals(dockerCommandOption)) {
            return registryTaskExporter;
        }

        throw new IllegalArgumentException("No exporter for option " + dockerCommandOption);
    }

    @NotNull
    @Override
    public TaskDefinitionExporter createExporter(@NotNull TaskProperties taskProperties) {
        if (Narrow.downTo(taskProperties, DockerBuildImageTaskProperties.class) != null) {
            return buildImageTaskExporter;
        }
        if (Narrow.downTo(taskProperties, DockerRunContainerTaskProperties.class) != null) {
            return runContainerTaskExporter;
        }

        if (Narrow.downTo(taskProperties, DockerRegistryTaskProperties.class) != null) {
            return registryTaskExporter;
        }

        throw new IllegalArgumentException("Can't export properties: " + taskProperties.getClass());
    }
}
