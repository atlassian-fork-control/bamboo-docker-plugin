package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import org.jetbrains.annotations.NotNull;

public interface DockerServiceFactory
{
    @NotNull
    DockerService create(@NotNull final Docker docker, @NotNull final String dockerCommandOption) throws IllegalArgumentException;
}
