package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import org.jetbrains.annotations.NotNull;

public interface DockerService
{
    void execute(@NotNull final CommonTaskContext taskContext) throws TaskException;
}
